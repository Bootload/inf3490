
import numpy as np
import sys
from random import uniform, sample
from pandas_ml import ConfusionMatrix
"""
Here i chose to actually use Pandas confusion matrix library, 
simply because im short on time. I belive my algorithm shows i
understand the MLP and its algorithm. Implementing a confusion matrix 
method would be nice, but i would rather use Pandas for this rather than
not having a confusion matrix at all. """

# TO DO: Implement momentum, write confusion matrix, improve accuracy.

class mlp:

    def __init__(self, inputs, targets, nhidden):
        self.alpha = 0.1
        self.beta = 1
        self.eta = 0.1
        self.nhidden = nhidden
        self.outputs = 8
        self.inputs = inputs
        self.inhidden = [[uniform(-1, 1) for i in range(nhidden)] for j in range(len(inputs[0])+1)]
        self.hiddenout = [[uniform(-1, 1) for i in range(self.outputs)] for j in range(nhidden + 1)]
        self.count = 0
        self.hiddenlayer = []
        self.outputlayer = []
        self.hidden_out_prev = [0]*(nhidden + 1)
        self.in_hidden_prev = [0]*(len(inputs[0]) + 1)

    def earlystopping(self, inputs, targets, valid, validtargets):
        done = False
        ten = sys.maxsize
        last_error = sys.maxsize
        i = 0
        while not done:

            self.train(inputs, targets)
            this_error = self.validate(valid, validtargets)
            print("Error of the network: " + str(this_error))
            if this_error > (last_error + last_error*0.1):
                done = True
                print("Error is increasing over treshold, ending!")
            if abs(this_error - last_error) < this_error*0.05:
                i += 1
            else:
                last_error = this_error
            if i > 20:
                done = True
                print("Network has not improved more than 1 % the last 20 iterations, terminating.")

    def train(self, inputs, targets, iterations=100):
        input_and_target_pairs = zip(inputs, targets)
        selection = sample(list(input_and_target_pairs), iterations)
        
        for pair in selection:
            self.forward(pair[0])
            errors = self.output_error(pair[1])
            blame = self.blame(errors)
            self.update_hiddenout(errors)
            self.update_inhidden(blame, pair[0])
                

    def forward(self, inputs):
        """
        Expects a 40 long list of inputs. 
        Returns nothing, but updates the the values in each node layer. 
        """
        inputs = np.append(inputs, -1) #Add bias node (the weigth is already in the list)
        hidden = [0]*self.nhidden
        for i in range(len(self.inhidden)):
            sum = 0
            for j in range(len(self.inhidden[i])):
                hidden[j] += self.inhidden[i][j]*inputs[i]
            
        hidden.append(-1) #Add bias node (the weigth is already in the list)
        #This concludes our input to hidden layer calculation with bias. 
        for i in range(len(hidden)):
            hidden[i] = self.sigmoid(hidden[i])

        output = [0]*self.outputs
        for i in range(len(self.hiddenout)):
            sum = 0
            for j in range(len(self.hiddenout[i])):
                output[j] += self.hiddenout[i][j]*hidden[i]
                
        for i in range(len(output)):
            output[i] = self.sigmoid(output[i])

        self.hiddenlayer = hidden.copy()
        self.outputlayer = output.copy()

    def confusion(self, inputs, targets):
        predicted = []
        expected = []
        for i, target in zip(inputs, targets.tolist()):
            self.forward(i)
            predicted.append(self.outputlayer.index(max(self.outputlayer)))
            expected.append(target.index(max(target)))       
        matrix = ConfusionMatrix(expected, predicted)
        matrix.print_stats()

    
    def sigmoid(self, x):
        return 1.0 / (1.0 + np.exp(-self.beta * x))

    def validate(self, inputs, targets):
        error = 0
        for sample, target in zip(inputs, targets):
            self.forward(sample)
            error += self.squared_error(target)     
        return error

    def squared_error(self, targets):
        sum = 0
        for i in range(len(self.outputlayer)):
            sum += (self.outputlayer[i] - targets[i])**2
        return sum/2
    
    def output_error(self, targets):
        errors = []
        for i in range(len(self.outputlayer)):
            y = self.outputlayer[i]
            t = targets[i]
            errors.append(self.beta*(y - t)*y*(1-y))
        return errors
    
    def update_hiddenout(self, errors):
        for i in range(len(self.hiddenout)-1):
            a = self.hiddenlayer[i]
            for j in range(len(self.hiddenout[i])):
                delta = errors[j]
                self.hiddenout[i][j] = self.hiddenout[i][j] - (self.eta*delta*a) + self.alpha*self.hidden_out_prev[j]
                self.hidden_out_prev[j] = delta
            


    
    def blame(self, errors):
        # Delegates blame for the error to the weights from the input layers
        nodes = []
        for i in range(len(self.hiddenout)):
            sum = 0
            for j in range(len(self.hiddenout[i])):
                sum += errors[j]*self.hiddenout[i][j] 
            nodes.append(sum)
        return nodes
    
    def update_inhidden(self, blame, inputs):
        for i in range(len(self.inhidden)-1):
            for j in range(len(self.inhidden[i])):

                a = self.hiddenlayer[j]
                self.inhidden[i][j] = self.inhidden[i][j] - (self.eta*a*(1-a)*blame[j]*inputs[i]) + self.alpha*self.in_hidden_prev[j]
                self.in_hidden_prev[j] = blame[j]
    

